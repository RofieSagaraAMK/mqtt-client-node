package com.sagara.ayamclient

import android.app.IntentService
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.IBinder
import android.util.Log
import com.sagara.ayamclient.encryt.Encrypted
import org.eclipse.paho.client.mqttv3.*
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import org.json.JSONObject
import java.io.IOException
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import java.net.URL
import java.nio.charset.Charset

abstract class AyamService: IntentService("AyamService") {
    @Volatile
    private var mqttClient: IMqttAsyncClient? = null

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onHandleIntent(p0: Intent?) {
        Log.d(TAG, "onHandleIntent")
        doConnect()
    }

    abstract fun onMessageReceived(message: Message)

    private fun doConnect() {
        var token: IMqttToken
        val options = MqttConnectOptions()
        options.isCleanSession = false
        options.isAutomaticReconnect = true
        try {
            mqttClient = MqttAsyncClient(SERVER_URL, DEVICE_ID, MemoryPersistence())
            mqttClient!!.setCallback(MqttEventCallback())
            token = mqttClient!!.connect(options)
            token.waitForCompletion(3500)
            token = mqttClient!!.subscribe(DEVICE_ID, 0)
            token.waitForCompletion(5000)
        } catch (ex: MqttSecurityException) {
            ex.printStackTrace()
        } catch (ex: MqttException) {
            when (ex.reasonCode) {
                MqttException.REASON_CODE_SERVER_CONNECT_ERROR.toInt() -> {
                    Log.v(TAG, "c${ex.message}")
                    ex.printStackTrace()
                }
                else -> {
                    Log.e(TAG, "a${ex.message}")
                    ex.printStackTrace()
                }
            }
        }
    }

    companion object {
        var SERVER_URL = "tcp://192.168.100.114:1883"
        var DEVICE_ID = "Rofie"
        private const val TAG = "AyamService"
    }

    inner class MqttEventCallback : MqttCallbackExtended {
        override fun connectionLost(cause: Throwable?) {
            Log.e(TAG, "Mqtt connectionLost")
        }

        override fun deliveryComplete(token: IMqttDeliveryToken?) {
            Log.v(TAG, "Token $token")
        }

        override fun connectComplete(reconnect: Boolean, serverURI: String?) {
            Log.i(TAG, "connectComplete lets request pending message from server!")
            val token = mqttClient!!.publish("request/$DEVICE_ID", MqttMessage())
            token.actionCallback = object : IMqttActionListener {
                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.e(TAG, "Error request pending message cause: ${exception?.message}")
                    exception?.printStackTrace()
                }

                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.i(TAG, "Request pending message is successfully!")
                }
            }
        }

        override fun messageArrived(topic: String?, message: MqttMessage?) {
            try {
                val payload = message?.payload?.toString(Charsets.UTF_8) ?: ""
                val plainMessage = Encrypted.decrypt(payload)
                val uid = JSONObject(plainMessage).getString("_id")
                val token = mqttClient!!
                    .publish("report/$DEVICE_ID", MqttMessage(Encrypted.encrypt(uid).toByteArray()))
                token.waitForCompletion(3500)
                Log.v(TAG, "Topic $topic, Message $payload")

                val contentJson = JSONObject(plainMessage)
                val messageContent = Message(
                    contentJson.getString("_id"),
                    contentJson.getString("target"),
                    contentJson.getString("message")
                )
                onMessageReceived(messageContent)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
    }
}