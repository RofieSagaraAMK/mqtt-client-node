package com.sagara.ayamclient

import com.sagara.ayamclient.encryt.Encrypted
import java.util.*

class AyamConfig private constructor(){
    private var _deviceId = UUID.randomUUID().toString()
    private var _url = "tcp://localhost:1883"
    private var _key = "default"

    val deviceId get() = _deviceId
    val url get() = _url

    fun setDeviceId(deviceId: String): AyamConfig {
        return this.apply { _deviceId = deviceId }
    }

    fun setUrl(url: String): AyamConfig {
        return this.apply { _url = url }
    }

    fun setKey(key: String): AyamConfig {
        return this.apply { _key = key }
    }

    @Deprecated("Use initService for service and client to get client class")
    fun initialize(): AyamConfig {
        AyamService.DEVICE_ID = _deviceId
        AyamService.SERVER_URL = _url
        Encrypted.KEY = _key
        return this
    }

    fun client(): AyamClient {
        Encrypted.KEY = _key
        return AyamClient().setConfig(this)
    }

    fun client(onMessageReceived: (Message) -> Unit): AyamClient {
        Encrypted.KEY = _key
        return AyamClient().setConfig(this, onMessageReceived)
    }

    fun initService(): AyamConfig {
        AyamService.DEVICE_ID = _deviceId
        AyamService.SERVER_URL = _url
        Encrypted.KEY = _key
        return this
    }

    companion object {
        fun builder(): AyamConfig {
            return AyamConfig()
        }
    }
}

class Message(val id: String, val target: String, val payload: String)