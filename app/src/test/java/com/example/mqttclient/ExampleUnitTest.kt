package com.example.mqttclient

import org.junit.Test

import org.junit.Assert.*
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        val inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US)
        val outputFormatter = DateTimeFormatter.ofPattern("MM d | E | hh:mm a", Locale.US)
        val date = OffsetDateTime.parse("2019-12-25T17:00:00-05:00")
        val formattedDate = outputFormatter.format(date)
        print(formattedDate)
    }
}
