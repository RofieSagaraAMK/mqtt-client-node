package com.sagara.ayamclient.encryt

class Encrypted {

    companion object {
        var KEY = "%C*F-JaNcRfUjXn2"

        fun decrypt(message: String): String {
            return Aes256.decrypt(message, KEY)
        }

        fun encrypt(message: String): String {
            return Aes256.encrypt(message, KEY)
        }
    }

}