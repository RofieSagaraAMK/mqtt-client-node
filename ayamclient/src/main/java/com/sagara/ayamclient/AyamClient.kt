package com.sagara.ayamclient

import android.util.Log
import com.sagara.ayamclient.encryt.Encrypted
import org.eclipse.paho.client.mqttv3.*
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import org.json.JSONObject

open class AyamClient {
    @Volatile
    private var mqttClient: IMqttAsyncClient? = null
    private var onMessageReceived: ((Message) -> Unit)? = null
    private var serverUrl = "tcp://192.168.100.114:1883"
    private var deviceId = "Rofie"

    companion object {
        private const val TAG = "AyamClient"
    }

    fun setConfig(config: AyamConfig, onMessageReceived: ((Message) -> Unit)? = null): AyamClient {
        serverUrl = config.url
        deviceId = config.deviceId
        this.onMessageReceived = onMessageReceived
        return this
    }

    fun setListener(onMessageReceived: (Message) -> Unit){
        this.onMessageReceived = onMessageReceived
    }

    fun removeListener(){
        this.onMessageReceived = null
    }

    fun doConnect() {
        val options = MqttConnectOptions()
        options.isCleanSession = false
        options.isAutomaticReconnect = true
        try {
            mqttClient =
                MqttAsyncClient(serverUrl, deviceId, MemoryPersistence())
            mqttClient!!.setCallback(MqttEventCallback())
            mqttClient!!.connect(options, null, object :IMqttActionListener{
                override fun onSuccess(asyncActionToken: IMqttToken?) {
                    Log.i(TAG, "Connected to MQTT server!")
                }

                override fun onFailure(asyncActionToken: IMqttToken?, exception: Throwable?) {
                    Log.e(TAG, "Error Subscribe cause: ${exception?.message}")
                    exception?.printStackTrace()
                }
            })
        } catch (ex: MqttSecurityException) {
            ex.printStackTrace()
        } catch (ex: MqttException) {
            when (ex.reasonCode) {
                MqttException.REASON_CODE_SERVER_CONNECT_ERROR.toInt() -> {
                    Log.v(TAG, "c${ex.message}")
                    ex.printStackTrace()
                }
                else -> {
                    Log.e(TAG, "a${ex.message}")
                    ex.printStackTrace()
                }
            }
        }
    }

    private inner class MqttEventCallback : MqttCallbackExtended {
        override fun connectionLost(cause: Throwable?) {
            Log.e(TAG, "Mqtt connectionLost")
        }

        override fun deliveryComplete(token: IMqttDeliveryToken?) {
            Log.v(TAG, "Token $token")
        }

        override fun connectComplete(reconnect: Boolean, serverURI: String?) {
            Log.i(TAG, "connectComplete lets request subscribe device to server!")
            mqttClient!!.subscribe(deviceId, 0, null, object : IMqttActionListener {
                override fun onSuccess(token: IMqttToken?) {
                    Log.i(TAG, "Subscribe to $deviceId successfully!")
                    Log.i(TAG, "Requesting pending message.")
                    requestPendingMessage()
                }

                override fun onFailure(token: IMqttToken?, exception: Throwable?) {
                    Log.e(TAG, "Error Subscribe cause: ${exception?.message}")
                    exception?.printStackTrace()
                }
            })
        }

        override fun messageArrived(topic: String?, message: MqttMessage?) {
            try {
                val payload = message?.payload?.toString(Charsets.UTF_8) ?: ""
                val plainMessage = Encrypted.decrypt(payload)
                val uid = JSONObject(plainMessage).getString("_id")
                mqttClient!!
                    .publish("report/${deviceId}", MqttMessage(Encrypted.encrypt(uid).toByteArray()))
                    .waitForCompletion()
                Log.v(TAG, "Topic $topic, Message $payload")

                val contentJson = JSONObject(plainMessage)
                val messageContent = Message(
                    contentJson.getString("_id"),
                    contentJson.getString("target"),
                    contentJson.getString("message")
                )
                onMessageReceived?.let { it(messageContent) }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
    }

    private fun requestPendingMessage() {
        mqttClient!!.publish("request/${deviceId}", MqttMessage(), null, object : IMqttActionListener {
                override fun onFailure(token: IMqttToken?, exception: Throwable?) {
                    Log.e(TAG, "Error request pending message cause: ${exception?.message}")
                    exception?.printStackTrace()
                }

                override fun onSuccess(token: IMqttToken?) {
                    Log.i(TAG, "Request pending message is successfully!")
                }
            })
    }
}