package com.example.mqttclient

import android.util.Log
import com.sagara.ayamclient.AyamService
import com.sagara.ayamclient.Message

class ImplementAyam: AyamService() {

    override fun onMessageReceived(message: Message) {
        Log.d("ImplementAyam", message.payload)
    }
}