package com.example.mqttclient

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sagara.ayamclient.AyamConfig
import com.sagara.ayamclient.AyamService
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textService.setOnClickListener {
            val client = AyamConfig.builder()
                .setDeviceId("Rofie")
                .setUrl("tcp://192.168.100.100:1883")
                .setKey("@NcRfUjXnZr4u7x!A%D*G-KaPdSgVkYp")
                .client()
            client.setListener {

            }
            client.doConnect()
            //startService(Intent(this, ImplementAyam::class.java))
        }
    }
}
